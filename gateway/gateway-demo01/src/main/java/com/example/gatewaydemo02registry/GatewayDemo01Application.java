package com.example.gatewaydemo02registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayDemo01Application {

    public static void main(String[] args) {
        SpringApplication.run(GatewayDemo01Application.class, args);
    }

}
