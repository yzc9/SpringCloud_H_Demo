package com.example.gatewaydemo02registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayDemo02RegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayDemo02RegistryApplication.class, args);
    }

}
