package com.example.gatewaydemo03config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayDemo03ConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayDemo03ConfigApplication.class, args);
    }

}
