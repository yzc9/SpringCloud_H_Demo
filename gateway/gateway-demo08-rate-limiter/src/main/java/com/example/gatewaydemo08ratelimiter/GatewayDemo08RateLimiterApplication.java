package com.example.gatewaydemo08ratelimiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayDemo08RateLimiterApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayDemo08RateLimiterApplication.class, args);
    }

}
