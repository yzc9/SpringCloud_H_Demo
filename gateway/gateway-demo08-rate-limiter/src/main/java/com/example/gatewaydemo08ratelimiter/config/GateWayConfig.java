package com.example.gatewaydemo08ratelimiter.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/9/22 16:17
 */
@Configuration
public class GateWayConfig {
    @Bean
    public KeyResolver ipKeyResolver() {
        return new KeyResolver() {
            
            @Override
            public Mono<String> resolve(ServerWebExchange exchange) {
                // 获取请求的 IP
                return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
            }
            
        };
    }
}