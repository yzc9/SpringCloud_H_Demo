package com.example.hystrixdemo01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableCircuitBreaker   //声明开启断路器功能
@EnableFeignClients    //声明开启FeignClient
public class HystrixDemo01Application {
    
    public static void main(String[] args) {
        SpringApplication.run(HystrixDemo01Application.class, args);
    }
    
}
