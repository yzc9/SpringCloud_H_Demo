package com.example.hystrixdemo01.controller;

import com.example.hystrixdemo01.feign.UserServiceFeignClient;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.exception.HystrixTimeoutException;
import feign.hystrix.FallbackFactory;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/9/16 11:10
 */
@RestController
@RequestMapping("/consumer")
// @DefaultProperties(defaultFallback = "globalErrorHandler")
public class Demo01Controller {
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private UserServiceFeignClient userServiceFeignClient;
    
    @GetMapping("/ok")
    public String ok(@RequestParam("id") Integer id) {
        String result = userServiceFeignClient.ok(id);
        logger.info("consumer调用成功-->method:ok");
        return "consumer-->ok" + result;
    }
    
    
    // @HystrixCommand(fallbackMethod = "errorHandler")
    // @HystrixCommand(fallbackMethod = "TimeController",commandProperties = {
    //         //设置改段程序运行超时时间为1s
    //         @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "1000")
    // })
    // @HystrixCommand
    @GetMapping("/error")
    public String error(@RequestParam("id") Integer id) {
        String result = userServiceFeignClient.error(id);
        logger.info("consumer调用成功-->method:error");
        return "consumer-->error" + result;
    }
    
    // //error的fallback方法
    // public String errorHandler(Integer id, Throwable throwable) {
    //     logger.info(ExceptionUtils.getRootCauseMessage(throwable));
    //     if (throwable instanceof HystrixTimeoutException) {
    //         return "超时异常";
    //     }
    //     return "其他异常";
    // }
    //
    // //error超时控制的fallback
    // public String TimeController(Integer id){
    //     return "运行超时，超时时间1s";
    // }
    //
    // //全局fallback方法
    // public String globalErrorHandler(){
    //     return "系统异常！请通知管理员";
    // }
}