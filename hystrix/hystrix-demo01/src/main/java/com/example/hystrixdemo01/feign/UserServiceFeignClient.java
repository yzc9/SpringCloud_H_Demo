package com.example.hystrixdemo01.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @FeignClient 声明FeignClient客户端
 * name 设置客户端的名字
 * url  设置服务调用地址，这里目前没有引入注册中心
 * fallbackFactory  设置 fallback 工厂类。fallback 的作用是，用于在 HTTP 调动失败而抛出异常的时候，提供 fallback 处理逻辑。
 */
@Component
@FeignClient(name = "user-service", url = "http://127.0.0.1:18080",fallback = UserServiceFeignClientFallback.class)
public interface UserServiceFeignClient {
    //访问正常
    @GetMapping("/user/ok")
    String ok(@RequestParam("id") Integer id);
    
    //访问超时或运行错误
    @GetMapping("/user/error")
    String error(@RequestParam("id") Integer id);
}
