package com.example.hystrixdemo01.feign;

import org.springframework.stereotype.Component;

/**
 * @Description: UserServiceFeignClient接口对应的fallback实现类
 * @Author : yzc
 * @Date : 2020/9/18 14:27
 */
@Component  //feign接口的@FeignClient需要引用，需要注入到bean容器
public class UserServiceFeignClientFallback implements UserServiceFeignClient {
    //ok方法对应的fallback方法
    @Override
    public String ok(Integer id) {
        return null;
    }
    
    //error方法对应的fallbace方法
    @Override
    public String error(Integer id) {
        return "客户端或者服务端错误，请联系管理员!";
    }
}