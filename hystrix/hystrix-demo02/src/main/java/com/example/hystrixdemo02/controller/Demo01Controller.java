package com.example.hystrixdemo02.controller;


import com.example.hystrixdemo02.feign.UserServiceFeignClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/9/16 11:10
 */
@RestController
@RequestMapping("/consumer")
public class Demo01Controller {
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private UserServiceFeignClient userServiceFeignClient;
    
    @GetMapping("/ok")
    public String ok(@RequestParam("id") Integer id) {
        String result = userServiceFeignClient.ok(id);
        logger.info("consumer调用成功-->method:ok");
        return "consumer-->ok" + result;
    }
    
    
    @GetMapping("/error")
    public String error(@RequestParam("id") Integer id) {
        String result = userServiceFeignClient.error(id);
        logger.info("consumer调用成功-->method:error");
        return "consumer-->error" + result;
    }
    
    //服务熔断
    @GetMapping("/{id}")
    @HystrixCommand(fallbackMethod = "testFallback", commandProperties = {
            //详细配置信息在 HystrixCommandProperties 类
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 失败率达到多少后跳闸
    })
    public String test(@PathVariable("id") Integer id) {
        logger.info("id:{}",id);
        if (id < 1) {
            throw new RuntimeException("id不能小于1");
        }
        String str = UUID.randomUUID().toString();
        return "调用成功，流水号：" + str;
    }
    
    //test的fallback方法
    public String testFallback(Integer id) {
        return "系统出错，请稍后再试";
    }
}