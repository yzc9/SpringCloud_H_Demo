package com.example.hystrixuserservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class HystrixUserServiceApplication {
    
    private Logger logger= LoggerFactory.getLogger(getClass());
    
    @RestController
    @RequestMapping("/user")
    public class UserController {
        //访问正常
        @GetMapping("/ok")
        public String ok(@RequestParam("id") Integer id) {
            logger.info("user-service-->method:ok");
            return "user-service-->ok";
        }
        
        //访问超时或运行错误
        @GetMapping("/error")
        public String error(@RequestParam("id") Integer id) {
            logger.info("user-service-->method:error");
            // int a=10/0;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "user-service-->error";
        }
        
        
    }
    
    public static void main(String[] args) {
        // 设置端口
        System.setProperty("server.port", "18080");
        
        // 应用启动
        SpringApplication.run(HystrixUserServiceApplication.class, args);
    }
    
}

