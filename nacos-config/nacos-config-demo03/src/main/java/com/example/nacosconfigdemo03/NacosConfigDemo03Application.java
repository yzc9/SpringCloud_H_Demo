package com.example.nacosconfigdemo03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NacosConfigDemo03Application {
    
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigDemo03Application.class, args);
    }
    
}
