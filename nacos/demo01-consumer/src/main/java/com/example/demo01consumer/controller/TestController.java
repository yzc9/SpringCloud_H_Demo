package com.example.demo01consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/8/24 15:23
 */
@RestController
public class TestController {
    @Autowired
    // cloud中服务发现统一接口
    private DiscoveryClient discoveryClient;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    // cloud中负载均衡统一接口
    private LoadBalancerClient loadBalancerClient;
    
    @GetMapping("/hello")
    public String hello(String name) {
        // <1> 获得服务 `demo01-provider` 的一个实例
        ServiceInstance instance;
        if (true) {
            // 获取服务 `demo01-provider` 对应的实例列表
            List<ServiceInstance> instances = discoveryClient.getInstances("demo01-provider");
            // 选择第一个
            instance = instances.size() > 0 ? instances.get(0) : null;
        } else {
            instance = loadBalancerClient.choose("demo01-provider");
        }
        // <2> 发起调用
        if (instance == null) {
            throw new IllegalStateException("获取不到实例");
        }
        String targetUrl = instance.getUri() + "/echo?name=" + name;
        String response = restTemplate.getForObject(targetUrl, String.class);
        // 返回结果
        return "consumer:" + response;
        
    }
    
}