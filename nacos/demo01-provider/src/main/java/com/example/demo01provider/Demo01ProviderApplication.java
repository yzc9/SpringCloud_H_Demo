package com.example.demo01provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient  //开启服务发现与主城功能，SpringVCloud E版本开始只需要引入 Spring Cloud 注册发现组件，就会自动开启注册发现的功能
public class Demo01ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Demo01ProviderApplication.class, args);
    }

}
