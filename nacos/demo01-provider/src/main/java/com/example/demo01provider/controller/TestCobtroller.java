package com.example.demo01provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/8/23 21:30
 */
@RestController
public class TestCobtroller {
    @GetMapping("/echo")
    public String echo(String name) {
        return "provider:" + name;
    }
}