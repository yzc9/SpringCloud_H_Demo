package com.example.demo02consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo02ConsumerApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(Demo02ConsumerApplication.class, args);
    }
    
}
