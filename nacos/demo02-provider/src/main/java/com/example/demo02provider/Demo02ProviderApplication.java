package com.example.demo02provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo02ProviderApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(Demo02ProviderApplication.class, args);
    }
    
}
