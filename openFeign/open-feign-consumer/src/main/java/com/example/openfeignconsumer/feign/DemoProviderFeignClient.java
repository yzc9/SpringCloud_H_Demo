package com.example.openfeignconsumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//声明 Feign 客户端。其中 name 属性，为 Feign 客户端的名字，也为 Ribbon 客户端的名字，也为注册中心的服务的名字。
@FeignClient(name = "open-feign-provider")
//@FeignClient(name = "iocoder", url = "http://www.iocoder.cn") // 注意，保持 name 属性和 url 属性的 host 是一致的
public interface DemoProviderFeignClient {
    // 添加 SpringMVC 注解，实现对 GET /demo 接口的声明式调用。
    @GetMapping("/echo")
    String echo(@RequestParam("name") String name);
    
    //单独使用
//    @GetMapping("/") // 请求首页
//    String echo(@RequestParam("name") String name);
    
}