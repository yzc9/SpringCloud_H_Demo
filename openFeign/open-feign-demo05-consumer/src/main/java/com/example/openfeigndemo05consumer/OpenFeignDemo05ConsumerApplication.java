package com.example.openfeigndemo05consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OpenFeignDemo05ConsumerApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignDemo05ConsumerApplication.class, args);
    }
    
}
