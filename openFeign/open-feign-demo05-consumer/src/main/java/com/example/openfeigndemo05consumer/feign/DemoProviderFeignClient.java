package com.example.openfeigndemo05consumer.feign;

import com.example.openfeigndemo05consumer.dto.DemoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

//声明 Feign 客户端。其中 name 属性，为 Feign 客户端的名字，也为 Ribbon 客户端的名字，也为注册中心的服务的名字。
@FeignClient(name = "open-feign-demo05-provider")
//@FeignClient(name = "iocoder", url = "http://www.iocoder.cn") // 注意，保持 name 属性和 url 属性的 host 是一致的
public interface DemoProviderFeignClient {
    
    @GetMapping("/get_demo") // GET 方式一，最推荐
    DemoDTO getDemo(@SpringQueryMap DemoDTO demoDTO);
    
    @GetMapping("/get_demo") // GET 方式二，相对推荐
    DemoDTO getDemo(@RequestParam("username") String username, @RequestParam("password") String password);
    
    @GetMapping("/get_demo") // GET 方式三，不推荐
    DemoDTO getDemo(@RequestParam Map<String, Object> params);
    
    @PostMapping("/post_demo") // POST 方式
    DemoDTO postDemo(@RequestBody DemoDTO demoDTO);
    
    
    //单独使用
    @GetMapping("/") // 请求首页
    String echo(@RequestParam("name") String name);
    
}