package com.example.openfeigndemo05provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenFeignDemo05ProviderApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignDemo05ProviderApplication.class, args);
    }
    
}
