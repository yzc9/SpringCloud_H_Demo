package com.example.openfeigndemo05provider.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Description:
 * @Author : yzc
 * @Date : 2020/8/25 15:24
 */
@Getter
@Setter
@ToString
public class DemoDTO {
    private String username;
    private String password;
}