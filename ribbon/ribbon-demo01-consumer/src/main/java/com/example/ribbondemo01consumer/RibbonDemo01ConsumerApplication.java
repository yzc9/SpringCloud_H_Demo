package com.example.ribbondemo01consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RibbonDemo01ConsumerApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(RibbonDemo01ConsumerApplication.class, args);
    }
    
    
    @Configuration
    public class RestTemplateConfiguration {
        
        @Bean
        @LoadBalanced   //声明 RestTemplate Bean 被配置使用 Spring Cloud LoadBalancerClient（负载均衡客户端），
        // 实现在请求目标服务时，能够进行负载均衡。
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
        
    }
    
    @RestController
    static class TestController {
        
        @Autowired
        private RestTemplate restTemplate;
        @Autowired
        private LoadBalancerClient loadBalancerClient;
        
        @GetMapping("/hello")
        public String hello(String name) {
            // 获得服务 `ribbon-demo01-provider` 的一个实例
            ServiceInstance instance = loadBalancerClient.choose("ribbon-demo01-provider");
            // 发起调用
            String targetUrl = instance.getUri() + "/echo?name=" + name;
            String response = restTemplate.getForObject(targetUrl, String.class);
            // 返回结果
            return "consumer:" + response;
        }
        
        @GetMapping("/hello02")
        public String hello02(String name) {
            // 直接使用 RestTemplate 调用服务 `ribbon-demo01-provider`
            String targetUrl = "http://ribbon-demo01-provider/echo?name=" + name;
            String response = restTemplate.getForObject(targetUrl, String.class);
            // 返回结果
            return "consumer:" + response;
        }
        
    }
    
}
